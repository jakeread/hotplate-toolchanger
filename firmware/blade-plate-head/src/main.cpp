#include <Arduino.h>
#include "indicators.h"
#include "config.h"
#include "drivers/servo.h"

#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"
#include "osape_arduino/vp_arduinoSerial.h"
#include "osape_ucbus/vb_ucBusDrop.h"

OSAP osap("blade-plate-head");

VPort_ArduinoSerial vpUSBSer(&osap, "arduinoUSBSerial", &Serial);

VBus vb_ucBusDrop(
  &osap, "ucBusDrop", 
  &vb_ucBusDrop_loop,
  &vb_ucBusDrop_send,
  &vb_ucBusDrop_cts
);

// servo_pwm is on PA08, TCC0-0 

EP_ONDATA_RESPONSES onServoPositionData(uint8_t* data, uint16_t len){
  chunk_uint32 microseconds = { .bytes = { data[0], data[1], data[2], data[3] } };
  servo_writeMicroseconds(microseconds.u);
  INDLIGHT_TOGGLE;
  return EP_ONDATA_ACCEPT;
}

Endpoint servoPositionEP(&osap, "servo current", onServoPositionData);

void setup() {
  CLKLIGHT_SETUP;
  BUSLIGHT_SETUP;
  INDLIGHT_SETUP;
  // thing 
  vpUSBSer.begin();
  vb_ucBusDrop_setup(&vb_ucBusDrop, false, 5);
  // the servo driver  
  servo_setup();
}

#define CLK_TICK 125
unsigned long last_tick = 0;

void loop() {
  // the osap main bb 
  osap.loop();
  // blink 
  /*
  if(millis() > last_tick + CLK_TICK){
    BUSLIGHT_TOGGLE;
    last_tick = millis();
  }
  */
}

void ucBusDrop_onPacketARx(uint8_t* inBufferA, volatile uint16_t len){}

void ucBusDrop_onRxISR(void){}